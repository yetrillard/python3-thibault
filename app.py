"""Flask API with various endpoint"""
from flask import Flask, Response

app = Flask(__name__)

@app.get('/info')

def get_info() -> Response:
    """Returns API information. """
    info = {
        "API": "my-api",
        "Version": "0.0.1",
        "Teams": "OPS"
    }
    return info
    app.logger.debug("info=%s", info)
    return info

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)